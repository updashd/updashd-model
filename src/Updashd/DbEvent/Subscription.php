<?php
namespace Updashd\DbEvent;

class Subscription {
    protected $tableName;
    protected $events = [];

    /**
     * Subscription constructor.
     * @param string $tableName
     * @param array $events ['I', 'U', 'D']
     */
    public function __construct ($tableName, $events) {
        $this->setTableName($tableName);
        $this->setEvents($events);
    }

    /**
     * @return string
     */
    public function getTableName () {
        return $this->tableName;
    }

    /**
     * @param string $tableName
     * @return $this
     */
    public function setTableName ($tableName) {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return array
     */
    public function getEvents () {
        return $this->events;
    }

    /**
     * @param array $events
     * @return $this
     */
    public function setEvents ($events) {
        $this->events = $events;
        return $this;
    }

}