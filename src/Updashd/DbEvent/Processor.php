<?php
namespace Updashd\DbEvent;

class Processor {
    protected $connection;
    protected $clientId;
    protected $tmpTableName;
    protected $populateStatements;

    /**
     * @param \Doctrine\DBAL\Connection|\PDO $connection
     * @param string $clientId
     * @param string $tmpTableName
     * @param Subscriptions $subscriptions
     */
    public function __construct ($connection, $clientId, $tmpTableName, $subscriptions) {
        $this->setConnection($connection);
        $this->setClientId($clientId);
        $this->setTmpTableName($tmpTableName);

        $this->setupVariables();
        $this->setupSubscriptions($subscriptions);
        $this->setupTemporaryTable();
    }

    protected function setupVariables () {
        $pdo = $this->getConnection();

        $pdo->exec("SET @clientId = '" .  $this->getClientId() . "'");
        $pdo->exec("SET @tmpTableName = '" .  $this->getTmpTableName() . "'");
    }

    /**
     * @param Subscriptions $subscriptions
     * @param bool $deleteOld
     */
    protected function setupSubscriptions ($subscriptions, $deleteOld = false) {
        $pdo = $this->getConnection();

        if ($deleteOld) {
            $this->unsubscribe();
        }

        //////////////////////////////////////////////////
        /// Set up the subscriptions
        //////////////////////////////////////////////////

        $stmt = $pdo->prepare("
            INSERT IGNORE INTO zz_subscription 
            (client_id, db_table_name, db_event_type) 
            VALUES
            (@clientId, :dbTableName, :dbEventType)
        ");

        foreach ($subscriptions->getSubscriptions() as $subscription) {
            foreach ($subscription->getEvents() as $event) {
                $stmt->execute([
                    'dbTableName' => $subscription->getTableName(),
                    'dbEventType' => $event
                ]);
            }
        }
    }

    protected function setupTemporaryTable () {
        $pdo = $this->getConnection();

        //////////////////////////////////////////////////
        /// Set up the temporary table
        //////////////////////////////////////////////////

        $pdo->exec("DROP TEMPORARY TABLE IF EXISTS " . $this->getTmpTableName());

        $pdo->exec("
            CREATE TEMPORARY TABLE " . $this->getTmpTableName() . " (
                root_id INT(11) UNSIGNED NOT NULL,
                db_table_name VARCHAR(100) NOT NULL,
                db_event_type CHAR(1) DEFAULT 'U' NOT NULL,
                event_id INT(11) UNSIGNED NOT NULL
            )
            CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci
        ");
    }

    public function unsubscribe () {
        $pdo = $this->getConnection();
        $pdo->exec("DELETE FROM zz_subscription WHERE client_id = @clientId");
    }

    public function populate () {
        $pdo = $this->getConnection();
        $populateStatements = $this->getPopulateStatements();

        $pdo->exec("TRUNCATE " . $this->getTmpTableName());

        $finalStatements = [];

        foreach ($populateStatements->getStatements() as $populateStatement) {
            $populateStatement->check(true);

            $finalStatements[] = "
            
                SELECT 
                    " . $populateStatement->getDestinationField() . ",
                    '" . $populateStatement->getTableName() . "',
                    se.db_event_type, 
                    se.event_id
                FROM zz_subscription_event se
                INNER JOIN " . $populateStatement->getAuditTableName() . " a ON se.zz_audit_id = a.event_id
                " . implode("\n", $populateStatement->getJoins()) . "
                WHERE 
                    se.db_table_name = '" . $populateStatement->getTableName() . "' AND 
                    se.client_id = @clientId AND 
                    se.db_event_type IN ('" . implode("', '", $populateStatement->getEvents()) . "')
                    
            ";
        };

        $pdo->exec("INSERT INTO " . $this->getTmpTableName() . " " . implode(' UNION ', $finalStatements));
    }

    /**
     * @param array $fields Fields to include in the SELECT ... FROM statement
     * @param array $joins Joins to make. r.root_id will be the distinct ID for the entity where a change has been detected.
     * @param string $rootField This is the name given to r.root_id in the select.
     * @param string $allFromTable If this is not null, the processor will simply update ALL records from the given table with id given by $rootField.
     * @return array The results of the query as an array with associative arrays representing rows.
     */
    public function retrieve ($fields, $joins = [], $rootField = 'root_id', $allFromTable = null) {
        $pdo = $this->getConnection();

        if ($allFromTable) {
            $idStmt = "SELECT DISTINCT " . $rootField . " AS root_id FROM " . $allFromTable;
        }
        else {
            $idStmt = "SELECT DISTINCT root_id FROM " . $this->getTmpTableName();
        }

        $rootField = 'r.root_id AS ' . $rootField;
        $fields = array_merge([$rootField], $fields);

        $sql = "
            SELECT
                " . implode(",\n", $fields) . "
            FROM (" . $idStmt . ") AS r
            " . implode("\n", $joins) . "
        ";

        $stmt = $pdo->prepare($sql);

        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    public function finalize () {
        $pdo = $this->getConnection();

        $pdo->exec("DELETE FROM zz_subscription_event WHERE event_id IN (SELECT DISTINCT event_id FROM " . $this->getTmpTableName() . ")");
    }

    /**
     * @return \PDO
     */
    protected function getConnection () {
        return $this->connection;
    }

    /**
     * @param \PDO $connection
     */
    protected function setConnection ($connection) {
        $this->connection = $connection;
    }

    /**
     * @return string
     */
    protected function getClientId () {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    protected function setClientId ($clientId) {
        $this->clientId = $clientId;
    }

    /**
     * @return string
     */
    protected function getTmpTableName () {
        return $this->tmpTableName;
    }

    /**
     * @param string $tmpTableName
     */
    protected function setTmpTableName ($tmpTableName) {
        $this->tmpTableName = $tmpTableName;
    }

    /**
     * @return PopulateStatements
     */
    public function getPopulateStatements () {
        return $this->populateStatements;
    }

    /**
     * @param PopulateStatements $populateStatements
     */
    public function setPopulateStatements ($populateStatements) {
        $this->populateStatements = $populateStatements;
    }
}
