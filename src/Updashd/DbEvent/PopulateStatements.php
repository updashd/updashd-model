<?php
namespace Updashd\DbEvent;

class PopulateStatements {
    /** @var PopulateStatement[] */
    protected $statements = [];

    /**
     * @param PopulateStatement $statement
     * @return $this
     */
    public function addStatement ($statement) {
        $this->statements[] = $statement;
        return $this;
    }

    /**
     * @param PopulateStatement[] $statements
     */
    public function setStatements ($statements) {
        $this->statements = $statements;
    }

    /**
     * @return PopulateStatement[]
     */
    public function getStatements () {
        return $this->statements;
    }
}