<?php
namespace Updashd\DbEvent;

class PopulateStatement {
    protected $destinationField;
    protected $tableName;
    protected $auditTableName;
    protected $joins = [];
    protected $events = [];

    public function __construct ($destinationField, $tableName, $auditTableName = null, $joins = [], $events = []) {
        $this->setDestinationField($destinationField);
        $this->setTableName($tableName);
        $this->setAuditTableName($auditTableName);
        $this->setJoins($joins);
        $this->setEvents($events);
    }

    public function check ($throwException = false) {
        $errors = [];

        if (! trim($this->getDestinationField())) {
            $errors[] = 'A destination field MUST be specified';
        }

        if (! trim($this->getTableName())) {
            $errors[] = 'A table name MUST be specified';
        }

        if (! trim($this->getAuditTableName())) {
            $errors[] = 'A audit table name MUST be specified';
        }

        if (count($this->getEvents()) == 0) {
            $errors[] = 'At least one event type MUST be provided';
        }

        if (count($errors)) {
            if ($throwException) {
                throw new \Exception('Cannot use PopulateStatement: ' . implode(' AND ', $errors));
            }
        }

        return $errors;
    }

    /**
     * @return string
     */
    public function getDestinationField () {
        return $this->destinationField;
    }

    /**
     * @param string $destinationField
     * @return $this
     */
    public function setDestinationField ($destinationField) {
        $this->destinationField = $destinationField;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableName () {
        return $this->tableName;
    }

    /**
     * @param string $tableName
     * @return $this
     */
    public function setTableName ($tableName) {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuditTableName () {
        return $this->auditTableName;
    }

    /**
     * @param string $auditTableName
     * @return $this
     */
    public function setAuditTableName ($auditTableName = null) {
        if (! $auditTableName) {
            $auditTableName = 'zz_audit_' . $this->getTableName();
        }

        $this->auditTableName = $auditTableName;
        return $this;
    }

    /**
     * @return array
     */
    public function getJoins () {
        return $this->joins;
    }

    /**
     * @param array $joins
     * @return $this
     */
    public function setJoins ($joins) {
        $this->joins = $joins;
        return $this;
    }

    /**
     * @return array
     */
    public function getEvents () {
        return $this->events;
    }

    /**
     * @param array $events
     * @return $this
     */
    public function setEvents ($events) {
        $this->events = $events;
        return $this;
    }

}