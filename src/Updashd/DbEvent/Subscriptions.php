<?php
namespace Updashd\DbEvent;


class Subscriptions {
    /** @var Subscription[] */
    protected $subscriptions = [];

    /**
     * @param string $tableName
     * @param array $events ['I', 'U', 'D']
     * @return $this
     */
    public function addSubscriptionAuto ($tableName, $events = ['I', 'U', 'D']) {
        $subscription = new Subscription($tableName, $events);
        $this->addSubscription($subscription);
        return $this;
    }

    /**
     * @param Subscription $subscription
     * @return $this
     */
    public function addSubscription ($subscription) {
        $this->subscriptions[] = $subscription;
        return $this;
    }

    /**
     * @param Subscription[] $subscriptions
     */
    public function setSubscriptions ($subscriptions) {
        $this->subscriptions = $subscriptions;
    }

    /**
     * @return Subscription[]
     */
    public function getSubscriptions () {
        return $this->subscriptions;
    }
}