<?php
namespace Updashd\Cli;

class Command {
    private $command;
    private $parameters = array();
    private $pipeFromFile = null;
    private $pipeToFile = null;
    private $defaultSeparator;
    private $statusCode;
    private $output;
    private $hasExecuted = false;
    
    /**
     * Create a command to make shell execution easier
     * @param string $commandName name of the command (eg. rsync)
     * @param string $defaultSeparator separator to use between flags and values. This will frequently be space, which is the default.
     */
    public function __construct ($commandName, $defaultSeparator = ' ') {
        $this->setCommand($commandName);
        $this->setDefaultSeparator($defaultSeparator);
    }
    
    /**
     * Add a custom parameter.
     * @param Parameter $parameter
     */
    public function addParameter (Parameter $parameter) {
        $this->parameters[] = $parameter;
    }
    
    /**
     * Add a flag to the parameters
     * @param string $flagName the flag (eg. -n or --name)
     */
    public function addParameterFlag ($flagName) {
        $parameter = new Parameter($flagName);
        $this->addParameter($parameter);
    }
    
    /**
     * Add a flag and value to the parameter
     * @param string $name the name of the flag (eg. --name or -n)
     * @param string $value the value to set the flag to
     * @param null $separator what to put between the flag and value. Uses default separator
     */
    public function addParameterFlagValue ($name, $value, $separator = null) {
        if ($separator === null) {
            $separator = $this->getDefaultSeparator();
        }
        
        $parameter = new Parameter($name, $value, Parameter::TYPE_FLAG_VALUE, $separator);
        $this->addParameter($parameter);
    }
    
    /**
     * Add a flag and a array to string value
     * @param string $name name of the flag (eg. --name or -n)
     * @param array $value the values to implode
     * @param string $glue the glue to use for implode
     * @param null $separator the separator to use between flag and values
     */
    public function addParameterFlagArray ($name, $value, $glue = ',', $separator = null) {
        if ($separator === null) {
            $separator = $this->getDefaultSeparator();
        }
        
        $parameter = new Parameter($name, $value, Parameter::TYPE_FLAG_ARRAY, $separator);
        $parameter->setGlue($glue);
        $this->addParameter($parameter);
    }
    
    /**
     * Add a string parameter (one that does not have a flag)
     * @param string $value the parameter value to add
     */
    public function addParameterString ($value) {
        $parameter = new Parameter(null, $value, Parameter::TYPE_STRING);
        $this->addParameter($parameter);
    }
    
    /**
     * Add a parameter value whose source is an array that needs to be imploded
     * @param array $value the values
     * @param string $glue the glue to use
     */
    public function addParameterArray ($value, $glue = ',') {
        $parameter = new Parameter(null, $value, Parameter::TYPE_ARRAY);
        $parameter->setGlue($glue);
        $this->addParameter($parameter);
    }
    
    /**
     * Get the command string that will be executed
     * @return string
     */
    public function getCommandString () {
        $parameters = array();
        
        foreach ($this->getParameters() as $parameter) {
            $parameters[] = $parameter->getParameterString();
        }
    
        $commandString = $this->getCommand() . ' ';
    
        $commandString .= implode(' ', $parameters);
        
        if ($this->getPipeFromFile()) {
            $commandString .= ' < ' . $this->getPipeFromFile();
        }
        
        if ($this->getPipeToFile()) {
            $commandString .= ' > ' . $this->getPipeToFile();
        }
        
        return $commandString;
    }
    
    /**
     * Execute the command, capturing output (retrievable through getOutput())
     * @param bool $throwExceptionOnError whether or not to throw an exception if the command returns non-zero (grenerally failure)
     * @return bool if the command was successful
     * @throws \Exception
     */
    public function execute ($throwExceptionOnError = false) {
        $status = 0;
        $output = array();
        
        exec($this->getCommandString(), $output, $status);
        
        $this->setHasExecuted(true);
        
        $this->setStatusCode($status);
    
        $this->setOutput($output);
        
        if ($status != 0 && $throwExceptionOnError) {
            throw new \Exception('Command "' . $this->getCommand() . '" returned non-success status.');
        }
        
        return ($status == 0);
    }
    
    /**
     * Execute the command, printing output to the terminal
     * @param bool $throwExceptionOnError whether or not to throw an exception if the command returns non-zero (grenerally failure)
     * @return bool if the command was successful
     * @throws \Exception
     */
    public function passthru ($throwExceptionOnError = false) {
        $status = 0;
    
        passthru($this->getCommandString(), $status);
        
        $this->setHasExecuted(true);
        
        $this->setStatusCode($status);
    
        if ($status != 0 && $throwExceptionOnError) {
            throw new \Exception('Command "' . $this->getCommand() . '" returned non-success status.');
        }
    
        return ($status == 0);
    }
    
    /**
     * Check if the command was successful
     * @return bool
     * @throws \Exception
     */
    public function wasSuccessful () {
        if (!$this->getHasExecuted()) {
            throw new \Exception('Trying to get output of a command that has not been executed yet.');
        }
    
        // The process was successful if the status code was zero (non-zero is error)
        return ($this->getStatusCode() == 0);
    }
    
    /**
     * Get the output of the command as a string
     * @param string $lineEnding
     * @return string
     */
    public function getOutputAsString ($lineEnding = PHP_EOL) {
        return implode($lineEnding, $this->getOutput());
    }
    
    /**
     * @return mixed
     */
    public function getCommand () {
        return $this->command;
    }
    
    /**
     * @param mixed $command
     */
    public function setCommand ($command) {
        $this->command = $command;
    }
    
    /**
     * @return Parameter[]
     */
    public function getParameters () {
        return $this->parameters;
    }
    
    /**
     * @param mixed $parameters
     */
    public function setParameters ($parameters) {
        $this->parameters = $parameters;
    }
    
    /**
     * @return string|null
     */
    public function getPipeFromFile () {
        return $this->pipeFromFile;
    }
    
    /**
     * @param string|null $pipeFromFile
     */
    public function setPipeFromFile ($pipeFromFile) {
        $this->pipeFromFile = $pipeFromFile;
    }
    
    /**
     * @return string|null
     */
    public function getPipeToFile () {
        return $this->pipeToFile;
    }
    
    /**
     * @param string|null $pipeToFile
     */
    public function setPipeToFile ($pipeToFile) {
        $this->pipeToFile = $pipeToFile;
    }
    
    /**
     * @return string
     */
    public function getDefaultSeparator () {
        return $this->defaultSeparator;
    }
    
    /**
     * @param string $defaultSeparator
     */
    public function setDefaultSeparator ($defaultSeparator) {
        $this->defaultSeparator = $defaultSeparator;
    }
    
    /**
     * @return int
     * @throws \Exception
     */
    public function getStatusCode () {
        if (!$this->getHasExecuted()) {
            throw new \Exception('Trying to get status code for a command that has not been executed yet.');
        }
    
        return $this->statusCode;
    }
    
    /**
     * @param int $statusCode
     */
    public function setStatusCode ($statusCode) {
        $this->statusCode = $statusCode;
    }
    
    /**
     * @return array
     * @throws \Exception
     */
    public function getOutput () {
        if (!$this->getHasExecuted()) {
            throw new \Exception('Trying to get output of a command that has not been executed yet.');
        }
        
        return $this->output;
    }
    
    /**
     * @param array $output
     */
    public function setOutput ($output) {
        $this->output = $output;
    }
    
    /**
     * @return boolean
     */
    public function getHasExecuted () {
        return $this->hasExecuted;
    }
    
    /**
     * @param boolean $hasExecuted
     */
    public function setHasExecuted ($hasExecuted) {
        $this->hasExecuted = $hasExecuted;
    }
}