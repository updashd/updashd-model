<?php
namespace Updashd\Doctrine;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class Manager {
    private $entityManager;

    public function __construct ($doctrineConfig) {
        $config = Setup::createAnnotationMetadataConfiguration(
            $doctrineConfig['options']['entity_paths'],
            $doctrineConfig['options']['dev_mode'],
            $doctrineConfig['options']['proxy_dir'],
            null, // Ignore cache for CLI
            $doctrineConfig['options']['simple_annotation']
        );

        Type::overrideType('datetime', UTCDateTimeType::class);
        Type::overrideType('datetimetz', UTCDateTimeType::class);

        $entityManager = EntityManager::create($doctrineConfig['connection'], $config);

        $this->setEntityManager($entityManager);
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager () {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager ($entityManager) {
        $this->entityManager = $entityManager;
    }
}