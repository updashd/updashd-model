<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * MetricType
 *
 * @ORM\Table(name="metric_type", indexes={@ORM\Index(name="metric_type_id", columns={"metric_type_id"}), @ORM\Index(name="metric_type_creator_id", columns={"creator_id"}), @ORM\Index(name="metric_type_updater_id", columns={"updater_id"})})
 * @ORM\Entity
 */
class MetricType extends \Updashd\Model\AbstractAuditedEntity
{
    const TYPE_FLOAT = 'FLOAT';
    const TYPE_INT = 'INT';
    const TYPE_STR = 'STR';
    const TYPE_TXT = 'TXT';

    /**
     * @var string
     *
     * @ORM\Column(name="metric_type_id", type="string", length=5, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $metricTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="metric_name", type="string", length=30, nullable=false)
     */
    private $metricName;



    /**
     * Get metricTypeId
     *
     * @return string
     */
    public function getMetricTypeId()
    {
        return $this->metricTypeId;
    }

    /**
     * Set metricName
     *
     * @param string $metricName
     *
     * @return MetricType
     */
    public function setMetricName($metricName)
    {
        $this->metricName = $metricName;

        return $this;
    }

    /**
     * Get metricName
     *
     * @return string
     */
    public function getMetricName()
    {
        return $this->metricName;
    }
}
