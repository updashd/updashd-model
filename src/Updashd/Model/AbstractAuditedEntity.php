<?php
namespace Updashd\Model;

use \Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
abstract class AbstractAuditedEntity {
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    protected $updatedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    protected $createdDate;

    /**
     * @var \Updashd\Model\Person
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Person")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="creator_id", referencedColumnName="person_id")
     * })
     */
    protected $creator;

    /**
     * @var \Updashd\Model\Person
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Person")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updater_id", referencedColumnName="person_id")
     * })
     */
    protected $updater;

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist () {
        $this->setCreatedDate(new \DateTime('now'));
        $this->setUpdatedDate(new \DateTime('now'));
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate () {
        $this->setUpdatedDate(new \DateTime('now'));
    }

    /**
     * Set updatedDate
     *
     * @param \DateTime $updatedDate
     *
     * @return AbstractAuditedEntity
     */
    public function setUpdatedDate ($updatedDate) {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * Get updatedDate
     *
     * @return \DateTime
     */
    public function getUpdatedDate () {
        return $this->updatedDate;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return AbstractAuditedEntity
     */
    public function setCreatedDate ($createdDate) {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate () {
        return $this->createdDate;
    }

    /**
     * Set updater
     *
     * @param \Updashd\Model\Person $updater
     *
     * @return AbstractAuditedEntity
     */
    public function setUpdater (\Updashd\Model\Person $updater = null) {
        $this->updater = $updater;

        return $this;
    }

    /**
     * Get updater
     *
     * @return \Updashd\Model\Person
     */
    public function getUpdater () {
        return $this->updater;
    }

    /**
     * Set creator
     *
     * @param \Updashd\Model\Person $creator
     *
     * @return AbstractAuditedEntity
     */
    public function setCreator (\Updashd\Model\Person $creator = null) {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \Updashd\Model\Person
     */
    public function getCreator () {
        return $this->creator;
    }
}
