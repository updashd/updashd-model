<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * ZzAuditMetricType
 *
 * @ORM\Table(name="zz_audit_metric_type")
 * @ORM\Entity
 */
class ZzAuditMetricType extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $eventId;

    /**
     * @var string
     *
     * @ORM\Column(name="event_type", type="string", length=1, nullable=false)
     */
    private $eventType = 'I';

    /**
     * @var string
     *
     * @ORM\Column(name="old_id", type="string", length=5, nullable=true)
     */
    private $oldId;

    /**
     * @var string
     *
     * @ORM\Column(name="new_id", type="string", length=5, nullable=true)
     */
    private $newId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="event_date", type="datetime", nullable=true)
     */
    private $eventDate;



    /**
     * Get eventId
     *
     * @return integer
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * Set eventType
     *
     * @param string $eventType
     *
     * @return ZzAuditMetricType
     */
    public function setEventType($eventType)
    {
        $this->eventType = $eventType;

        return $this;
    }

    /**
     * Get eventType
     *
     * @return string
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * Set oldId
     *
     * @param string $oldId
     *
     * @return ZzAuditMetricType
     */
    public function setOldId($oldId)
    {
        $this->oldId = $oldId;

        return $this;
    }

    /**
     * Get oldId
     *
     * @return string
     */
    public function getOldId()
    {
        return $this->oldId;
    }

    /**
     * Set newId
     *
     * @param string $newId
     *
     * @return ZzAuditMetricType
     */
    public function setNewId($newId)
    {
        $this->newId = $newId;

        return $this;
    }

    /**
     * Get newId
     *
     * @return string
     */
    public function getNewId()
    {
        return $this->newId;
    }

    /**
     * Set eventDate
     *
     * @param \DateTime $eventDate
     *
     * @return ZzAuditMetricType
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * Get eventDate
     *
     * @return \DateTime
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }
}
