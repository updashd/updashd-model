<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 *
 * @ORM\Table(name="service", indexes={@ORM\Index(name="service_name", columns={"service_name"}), @ORM\Index(name="service_module_name", columns={"module_name"}), @ORM\Index(name="service_account_id", columns={"account_id"}), @ORM\Index(name="service_updater_id", columns={"updater_id"}), @ORM\Index(name="service_creator_id", columns={"creator_id"})})
 * @ORM\Entity
 */
class Service extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="service_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $serviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="service_name", type="string", length=50, nullable=false)
     */
    private $serviceName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="module_name", type="string", length=20, nullable=false)
     */
    private $moduleName = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder = '0';

    /**
     * @var \Updashd\Model\Account
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     * })
     */
    private $account;

    /**
     * @var \Updashd\Model\ServiceMetricField[]
     *
     * @ORM\OneToMany(targetEntity="\Updashd\Model\ServiceMetricField", mappedBy="service")
     */
    private $serviceMetricFields;



    /**
     * Get serviceId
     *
     * @return integer
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * Set serviceName
     *
     * @param string $serviceName
     *
     * @return Service
     */
    public function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;

        return $this;
    }

    /**
     * Get serviceName
     *
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * Set moduleName
     *
     * @param string $moduleName
     *
     * @return Service
     */
    public function setModuleName($moduleName)
    {
        $this->moduleName = $moduleName;

        return $this;
    }

    /**
     * Get moduleName
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return Service
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set account
     *
     * @param \Updashd\Model\Account $account
     *
     * @return Service
     */
    public function setAccount(\Updashd\Model\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Updashd\Model\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return \Updashd\Model\ServiceMetricField[]
     */
    public function getServiceMetricFields () {
        return $this->serviceMetricFields;
    }
}
