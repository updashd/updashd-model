<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * ResultMetric
 *
 * @ORM\Table(name="result_metric", indexes={@ORM\Index(name="result_metric_creator_id", columns={"creator_id"}), @ORM\Index(name="result_metric_updater_id", columns={"updater_id"}), @ORM\Index(name="result_metric_result_id_field_name", columns={"result_id", "field_name"}), @ORM\Index(name="metric_type_creator_id", columns={"creator_id"}), @ORM\Index(name="metric_type_result_id_field_name", columns={"result_id", "field_name"}), @ORM\Index(name="metric_type_updater_id", columns={"updater_id"}), @ORM\Index(name="result_metric_type_id", columns={"metric_type_id"}), @ORM\Index(name="IDX_634915BF7A7B643", columns={"result_id"})})
 * @ORM\Entity
 */
class ResultMetric extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="result_metric_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $resultMetricId;

    /**
     * @var string
     *
     * @ORM\Column(name="field_name", type="string", length=30, nullable=true)
     */
    private $fieldName;

    /**
     * @var integer
     *
     * @ORM\Column(name="value_i", type="integer", nullable=true)
     */
    private $valueI;

    /**
     * @var string
     *
     * @ORM\Column(name="value_s", type="string", length=200, nullable=true)
     */
    private $valueS;

    /**
     * @var float
     *
     * @ORM\Column(name="value_f", type="float", precision=10, scale=0, nullable=true)
     */
    private $valueF;

    /**
     * @var string
     *
     * @ORM\Column(name="value_t", type="text", length=16777215, nullable=true)
     */
    private $valueT;

    /**
     * @var \Updashd\Model\Result
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Result")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="result_id", referencedColumnName="result_id")
     * })
     */
    private $result;

    /**
     * @var \Updashd\Model\MetricType
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\MetricType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="metric_type_id", referencedColumnName="metric_type_id")
     * })
     */
    private $metricType;



    /**
     * Get resultMetricId
     *
     * @return integer
     */
    public function getResultMetricId()
    {
        return $this->resultMetricId;
    }

    /**
     * Set fieldName
     *
     * @param string $fieldName
     *
     * @return ResultMetric
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * Get fieldName
     *
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * Set valueI
     *
     * @param integer $valueI
     *
     * @return ResultMetric
     */
    public function setValueI($valueI)
    {
        $this->valueI = $valueI;

        return $this;
    }

    /**
     * Get valueI
     *
     * @return integer
     */
    public function getValueI()
    {
        return $this->valueI;
    }

    /**
     * Set valueS
     *
     * @param string $valueS
     *
     * @return ResultMetric
     */
    public function setValueS($valueS)
    {
        $this->valueS = $valueS;

        return $this;
    }

    /**
     * Get valueS
     *
     * @return string
     */
    public function getValueS()
    {
        return $this->valueS;
    }

    /**
     * Set valueF
     *
     * @param float $valueF
     *
     * @return ResultMetric
     */
    public function setValueF($valueF)
    {
        $this->valueF = $valueF;

        return $this;
    }

    /**
     * Get valueF
     *
     * @return float
     */
    public function getValueF()
    {
        return $this->valueF;
    }

    /**
     * Set valueT
     *
     * @param string $valueT
     *
     * @return ResultMetric
     */
    public function setValueT($valueT)
    {
        $this->valueT = $valueT;

        return $this;
    }

    /**
     * Get valueT
     *
     * @return string
     */
    public function getValueT()
    {
        return $this->valueT;
    }

    public function getValueAuto () {
        $metricType = $this->getMetricType();
        $typeId = $metricType->getMetricTypeId();

        switch ($typeId) {
            case $metricType::TYPE_FLOAT: return $this->getValueF(); break;
            case $metricType::TYPE_INT: return $this->getValueI(); break;
            case $metricType::TYPE_STR: return $this->getValueS(); break;
            case $metricType::TYPE_TXT: return $this->getValueT(); break;
            default: return null;
        }
    }

    /**
     * Set the value using the metric type given (or that already on the object)
     * @param mixed $value
     * @param MetricType|null $metricType
     */
    public function setValueAuto ($value, $metricType = null) {
        if (! $metricType) {
            $metricType = $this->getMetricType();
        }

        if ($metricType) {
            switch ($metricType->getMetricTypeId()) {
                case $metricType::TYPE_FLOAT: $this->setValueF($value); break;
                case $metricType::TYPE_INT: $this->setValueI($value); break;
                case $metricType::TYPE_STR: $this->setValueS($value); break;
                case $metricType::TYPE_TXT: $this->setValueT($value); break;
                default: break;
            }
        }
    }

    /**
     * Set result
     *
     * @param \Updashd\Model\Result $result
     *
     * @return ResultMetric
     */
    public function setResult(\Updashd\Model\Result $result = null)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return \Updashd\Model\Result
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set metricType
     *
     * @param \Updashd\Model\MetricType $metricType
     *
     * @return ResultMetric
     */
    public function setMetricType(\Updashd\Model\MetricType $metricType = null)
    {
        $this->metricType = $metricType;

        return $this;
    }

    /**
     * Get metricType
     *
     * @return \Updashd\Model\MetricType
     */
    public function getMetricType()
    {
        return $this->metricType;
    }
}
