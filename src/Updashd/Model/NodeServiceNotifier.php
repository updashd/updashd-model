<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * NodeServiceNotifier
 *
 * @ORM\Table(name="node_service_notifier", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_node_service_notifier", columns={"node_service_id", "account_notifier_id"})}, indexes={@ORM\Index(name="nsn_node_service_id", columns={"node_service_id"}), @ORM\Index(name="nsn_zone_id", columns={"account_notifier_id"}), @ORM\Index(name="nsn_updater_id", columns={"updater_id"}), @ORM\Index(name="nsn_creator_id", columns={"creator_id"})})
 * @ORM\Entity
 */
class NodeServiceNotifier extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="node_service_notifier_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $nodeServiceNotifierId;

    /**
     * @var \Updashd\Model\NodeService
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\NodeService")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="node_service_id", referencedColumnName="node_service_id")
     * })
     */
    private $nodeService;

    /**
     * @var \Updashd\Model\AccountNotifier
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\AccountNotifier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_notifier_id", referencedColumnName="account_notifier_id")
     * })
     */
    private $accountNotifier;



    /**
     * Get nodeServiceNotifierId
     *
     * @return integer
     */
    public function getNodeServiceNotifierId()
    {
        return $this->nodeServiceNotifierId;
    }

    /**
     * Set nodeService
     *
     * @param \Updashd\Model\NodeService $nodeService
     *
     * @return NodeServiceNotifier
     */
    public function setNodeService(\Updashd\Model\NodeService $nodeService = null)
    {
        $this->nodeService = $nodeService;

        return $this;
    }

    /**
     * Get nodeService
     *
     * @return \Updashd\Model\NodeService
     */
    public function getNodeService()
    {
        return $this->nodeService;
    }

    /**
     * Set accountNotifier
     *
     * @param \Updashd\Model\AccountNotifier $accountNotifier
     *
     * @return NodeServiceNotifier
     */
    public function setAccountNotifier(\Updashd\Model\AccountNotifier $accountNotifier = null)
    {
        $this->accountNotifier = $accountNotifier;

        return $this;
    }

    /**
     * Get accountNotifier
     *
     * @return \Updashd\Model\AccountNotifier
     */
    public function getAccountNotifier()
    {
        return $this->accountNotifier;
    }
}
