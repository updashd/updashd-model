<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Setting
 *
 * @ORM\Table(name="setting", uniqueConstraints={@ORM\UniqueConstraint(name="setting_setting_key_uindex", columns={"setting_key"})}, indexes={@ORM\Index(name="setting_person_person_id_fk", columns={"creator_id"}), @ORM\Index(name="setting_person_updater_id_fk", columns={"updater_id"})})
 * @ORM\Entity
 */
class Setting extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="setting_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $settingId;

    /**
     * @var string
     *
     * @ORM\Column(name="setting_key", type="string", length=20, nullable=true)
     */
    private $settingKey;

    /**
     * @var string
     *
     * @ORM\Column(name="setting_name", type="string", length=50, nullable=true)
     */
    private $settingName;

    /**
     * @var string
     *
     * @ORM\Column(name="setting_description", type="string", length=255, nullable=true)
     */
    private $settingDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="setting_default", type="string", length=255, nullable=true)
     */
    private $settingDefault;



    /**
     * Get settingId
     *
     * @return integer
     */
    public function getSettingId()
    {
        return $this->settingId;
    }

    /**
     * Set settingKey
     *
     * @param string $settingKey
     *
     * @return Setting
     */
    public function setSettingKey($settingKey)
    {
        $this->settingKey = $settingKey;

        return $this;
    }

    /**
     * Get settingKey
     *
     * @return string
     */
    public function getSettingKey()
    {
        return $this->settingKey;
    }

    /**
     * Set settingName
     *
     * @param string $settingName
     *
     * @return Setting
     */
    public function setSettingName($settingName)
    {
        $this->settingName = $settingName;

        return $this;
    }

    /**
     * Get settingName
     *
     * @return string
     */
    public function getSettingName()
    {
        return $this->settingName;
    }

    /**
     * Set settingDescription
     *
     * @param string $settingDescription
     *
     * @return Setting
     */
    public function setSettingDescription($settingDescription)
    {
        $this->settingDescription = $settingDescription;

        return $this;
    }

    /**
     * Get settingDescription
     *
     * @return string
     */
    public function getSettingDescription()
    {
        return $this->settingDescription;
    }

    /**
     * Set settingDefault
     *
     * @param string $settingDefault
     *
     * @return Setting
     */
    public function setSettingDefault($settingDefault)
    {
        $this->settingDefault = $settingDefault;

        return $this;
    }

    /**
     * Get settingDefault
     *
     * @return string
     */
    public function getSettingDefault()
    {
        return $this->settingDefault;
    }
}
