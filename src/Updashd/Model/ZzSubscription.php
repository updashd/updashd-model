<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * ZzSubscription
 *
 * @ORM\Table(name="zz_subscription", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_event", columns={"client_id", "db_event_type", "db_table_name"})})
 * @ORM\Entity
 */
class ZzSubscription extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="subscription_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $subscriptionId;

    /**
     * @var string
     *
     * @ORM\Column(name="client_id", type="string", length=10, nullable=true)
     */
    private $clientId;

    /**
     * @var string
     *
     * @ORM\Column(name="db_table_name", type="string", length=50, nullable=true)
     */
    private $dbTableName;

    /**
     * @var string
     *
     * @ORM\Column(name="db_event_type", type="string", length=1, nullable=false)
     */
    private $dbEventType = 'I';



    /**
     * Get subscriptionId
     *
     * @return integer
     */
    public function getSubscriptionId()
    {
        return $this->subscriptionId;
    }

    /**
     * Set clientId
     *
     * @param string $clientId
     *
     * @return ZzSubscription
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId
     *
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set dbTableName
     *
     * @param string $dbTableName
     *
     * @return ZzSubscription
     */
    public function setDbTableName($dbTableName)
    {
        $this->dbTableName = $dbTableName;

        return $this;
    }

    /**
     * Get dbTableName
     *
     * @return string
     */
    public function getDbTableName()
    {
        return $this->dbTableName;
    }

    /**
     * Set dbEventType
     *
     * @param string $dbEventType
     *
     * @return ZzSubscription
     */
    public function setDbEventType($dbEventType)
    {
        $this->dbEventType = $dbEventType;

        return $this;
    }

    /**
     * Get dbEventType
     *
     * @return string
     */
    public function getDbEventType()
    {
        return $this->dbEventType;
    }
}
