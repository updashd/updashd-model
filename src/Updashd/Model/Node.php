<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Node
 *
 * @ORM\Table(name="node", indexes={@ORM\Index(name="node_name", columns={"node_name"}), @ORM\Index(name="node_environment_id", columns={"environment_id"}), @ORM\Index(name="node_sort_order", columns={"sort_order"}), @ORM\Index(name="node_account_id", columns={"account_id"}), @ORM\Index(name="node_account_environment", columns={"environment_id", "account_id"}), @ORM\Index(name="node_updater_id", columns={"updater_id"}), @ORM\Index(name="node_creator_id", columns={"creator_id"})})
 * @ORM\Entity
 */
class Node extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="node_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $nodeId;

    /**
     * @var string
     *
     * @ORM\Column(name="node_name", type="string", length=50, nullable=false)
     */
    private $nodeName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="hostname", type="string", length=200, nullable=false)
     */
    private $hostname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=45, nullable=false)
     */
    private $ip = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_enabled", type="boolean", nullable=false)
     */
    private $isEnabled = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder = '0';

    /**
     * @var \Updashd\Model\Account
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     * })
     */
    private $account;

    /**
     * @var \Updashd\Model\Environment
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Environment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="environment_id", referencedColumnName="environment_id")
     * })
     */
    private $environment;

    /**
     * @var \Updashd\Model\NodeService[]
     *
     * @ORM\OneToMany(targetEntity="\Updashd\Model\NodeService", mappedBy="node")
     */
    private $nodeServices;

    /**
     * Get nodeId
     *
     * @return integer
     */
    public function getNodeId()
    {
        return $this->nodeId;
    }

    /**
     * Set nodeName
     *
     * @param string $nodeName
     *
     * @return Node
     */
    public function setNodeName($nodeName)
    {
        $this->nodeName = $nodeName;

        return $this;
    }

    /**
     * Get nodeName
     *
     * @return string
     */
    public function getNodeName()
    {
        return $this->nodeName;
    }

    /**
     * Set hostname
     *
     * @param string $hostname
     *
     * @return Node
     */
    public function setHostname($hostname)
    {
        $this->hostname = $hostname;

        return $this;
    }

    /**
     * Get hostname
     *
     * @return string
     */
    public function getHostname()
    {
        return $this->hostname;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Node
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return Node
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return Node
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set account
     *
     * @param \Updashd\Model\Account $account
     *
     * @return Node
     */
    public function setAccount(\Updashd\Model\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Updashd\Model\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set environment
     *
     * @param \Updashd\Model\Environment $environment
     *
     * @return Node
     */
    public function setEnvironment(\Updashd\Model\Environment $environment = null)
    {
        $this->environment = $environment;

        return $this;
    }

    /**
     * Get environment
     *
     * @return \Updashd\Model\Environment
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @return \Updashd\Model\NodeService[]
     */
    public function getNodeServices () {
        return $this->nodeServices;
    }
}
