<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServiceMetricField
 *
 * @ORM\Table(name="service_metric_field", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_smf_service_field_name", columns={"service_id", "field_name"})}, indexes={@ORM\Index(name="smf_metric_type_id_fk", columns={"metric_type_id"}), @ORM\Index(name="smf_updater_id_fk", columns={"updater_id"}), @ORM\Index(name="smf_creator_id_fk", columns={"creator_id"}), @ORM\Index(name="IDX_705CF28ED5CA9E6", columns={"service_id"})})
 * @ORM\Entity
 */
class ServiceMetricField extends \Updashd\Model\AbstractAuditedEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="service_metric_field_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $serviceMetricFieldId;

    /**
     * @var string
     *
     * @ORM\Column(name="field_name", type="string", length=30, nullable=true)
     */
    private $fieldName;

    /**
     * @var string
     *
     * @ORM\Column(name="readable_name", type="string", length=250, nullable=false)
     */
    private $readableName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="unit", type="string", length=20, nullable=false)
     */
    private $unit = '';

    /**
     * @var \Updashd\Model\MetricType
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\MetricType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="metric_type_id", referencedColumnName="metric_type_id")
     * })
     */
    private $metricType;

    /**
     * @var \Updashd\Model\Service
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Service")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="service_id", referencedColumnName="service_id")
     * })
     */
    private $service;


    /**
     * Get serviceMetricFieldId
     *
     * @return integer
     */
    public function getServiceMetricFieldId () {
        return $this->serviceMetricFieldId;
    }

    /**
     * Set fieldName
     *
     * @param string $fieldName
     *
     * @return ServiceMetricField
     */
    public function setFieldName ($fieldName) {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * Get fieldName
     *
     * @return string
     */
    public function getFieldName () {
        return $this->fieldName;
    }

    /**
     * Set readableName
     *
     * @param string $readableName
     *
     * @return ServiceMetricField
     */
    public function setReadableName ($readableName) {
        $this->readableName = $readableName;

        return $this;
    }

    /**
     * Get readableName
     *
     * @return string
     */
    public function getReadableName () {
        return $this->readableName;
    }

    /**
     * Set unit
     *
     * @param string $unit
     *
     * @return ServiceMetricField
     */
    public function setUnit ($unit) {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return string
     */
    public function getUnit () {
        return $this->unit;
    }

    /**
     * Set metricType
     *
     * @param \Updashd\Model\MetricType $metricType
     *
     * @return ServiceMetricField
     */
    public function setMetricType (\Updashd\Model\MetricType $metricType = null) {
        $this->metricType = $metricType;

        return $this;
    }

    /**
     * Get metricType
     *
     * @return \Updashd\Model\MetricType
     */
    public function getMetricType () {
        return $this->metricType;
    }

    /**
     * Set service
     *
     * @param \Updashd\Model\Service $service
     *
     * @return ServiceMetricField
     */
    public function setService (\Updashd\Model\Service $service = null) {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \Updashd\Model\Service
     */
    public function getService () {
        return $this->service;
    }
}
