<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Severity
 *
 * @ORM\Table(name="severity", uniqueConstraints={@ORM\UniqueConstraint(name="severity_severity_id_uindex", columns={"severity_id"})}, indexes={@ORM\Index(name="sv_creator_id", columns={"creator_id"}), @ORM\Index(name="sv_updater_id", columns={"updater_id"})})
 * @ORM\Entity
 */
class Severity extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="severity_id", type="string", length=10, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $severityId;

    /**
     * @var string
     *
     * @ORM\Column(name="severity_name", type="string", length=250, nullable=false)
     */
    private $severityName;

    /**
     * @var string
     *
     * @ORM\Column(name="bootstrap_color", type="string", length=20, nullable=false)
     */
    private $bootstrapColor = 'warning';

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="updater_id", type="integer", nullable=true)
     */
    private $updaterId;

    /**
     * @var integer
     *
     * @ORM\Column(name="creator_id", type="integer", nullable=true)
     */
    private $creatorId;

    /**
     * @var string
     *
     * @ORM\Column(name="layout_color", type="string", length=20, nullable=false)
     */
    private $layoutColor = 'aqua';

    /**
     * @var string
     *
     * @ORM\Column(name="layout_icon", type="string", length=40, nullable=false)
     */
    private $layoutIcon = 'information-circled';

    /**
     * @var string
     *
     * @ORM\Column(name="layout_icon_vendor", type="string", length=10, nullable=false)
     */
    private $layoutIconVendor = 'ion';



    /**
     * Get severityId
     *
     * @return string
     */
    public function getSeverityId()
    {
        return $this->severityId;
    }

    /**
     * Set severityName
     *
     * @param string $severityName
     *
     * @return Severity
     */
    public function setSeverityName($severityName)
    {
        $this->severityName = $severityName;

        return $this;
    }

    /**
     * Get severityName
     *
     * @return string
     */
    public function getSeverityName()
    {
        return $this->severityName;
    }

    /**
     * Set bootstrapColor
     *
     * @param string $bootstrapColor
     *
     * @return Severity
     */
    public function setBootstrapColor($bootstrapColor)
    {
        $this->bootstrapColor = $bootstrapColor;

        return $this;
    }

    /**
     * Get bootstrapColor
     *
     * @return string
     */
    public function getBootstrapColor()
    {
        return $this->bootstrapColor;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return Severity
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set updaterId
     *
     * @param integer $updaterId
     *
     * @return Severity
     */
    public function setUpdaterId($updaterId)
    {
        $this->updaterId = $updaterId;

        return $this;
    }

    /**
     * Get updaterId
     *
     * @return integer
     */
    public function getUpdaterId()
    {
        return $this->updaterId;
    }

    /**
     * Set creatorId
     *
     * @param integer $creatorId
     *
     * @return Severity
     */
    public function setCreatorId($creatorId)
    {
        $this->creatorId = $creatorId;

        return $this;
    }

    /**
     * Get creatorId
     *
     * @return integer
     */
    public function getCreatorId()
    {
        return $this->creatorId;
    }

    /**
     * Set layoutColor
     *
     * @param string $layoutColor
     *
     * @return Severity
     */
    public function setLayoutColor($layoutColor)
    {
        $this->layoutColor = $layoutColor;

        return $this;
    }

    /**
     * Get layoutColor
     *
     * @return string
     */
    public function getLayoutColor()
    {
        return $this->layoutColor;
    }

    /**
     * Set layoutIcon
     *
     * @param string $layoutIcon
     *
     * @return Severity
     */
    public function setLayoutIcon($layoutIcon)
    {
        $this->layoutIcon = $layoutIcon;

        return $this;
    }

    /**
     * Get layoutIcon
     *
     * @return string
     */
    public function getLayoutIcon()
    {
        return $this->layoutIcon;
    }

    /**
     * Set layoutIconVendor
     *
     * @param string $layoutIconVendor
     *
     * @return Severity
     */
    public function setLayoutIconVendor($layoutIconVendor)
    {
        $this->layoutIconVendor = $layoutIconVendor;

        return $this;
    }

    /**
     * Get layoutIconVendor
     *
     * @return string
     */
    public function getLayoutIconVendor()
    {
        return $this->layoutIconVendor;
    }
}
