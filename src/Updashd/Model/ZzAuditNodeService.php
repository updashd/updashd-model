<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * ZzAuditNodeService
 *
 * @ORM\Table(name="zz_audit_node_service")
 * @ORM\Entity
 */
class ZzAuditNodeService extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $eventId;

    /**
     * @var string
     *
     * @ORM\Column(name="event_type", type="string", length=1, nullable=false)
     */
    private $eventType = 'I';

    /**
     * @var integer
     *
     * @ORM\Column(name="old_id", type="integer", nullable=true)
     */
    private $oldId;

    /**
     * @var integer
     *
     * @ORM\Column(name="new_id", type="integer", nullable=true)
     */
    private $newId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="event_date", type="datetime", nullable=true)
     */
    private $eventDate;



    /**
     * Get eventId
     *
     * @return integer
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * Set eventType
     *
     * @param string $eventType
     *
     * @return ZzAuditNodeService
     */
    public function setEventType($eventType)
    {
        $this->eventType = $eventType;

        return $this;
    }

    /**
     * Get eventType
     *
     * @return string
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * Set oldId
     *
     * @param integer $oldId
     *
     * @return ZzAuditNodeService
     */
    public function setOldId($oldId)
    {
        $this->oldId = $oldId;

        return $this;
    }

    /**
     * Get oldId
     *
     * @return integer
     */
    public function getOldId()
    {
        return $this->oldId;
    }

    /**
     * Set newId
     *
     * @param integer $newId
     *
     * @return ZzAuditNodeService
     */
    public function setNewId($newId)
    {
        $this->newId = $newId;

        return $this;
    }

    /**
     * Get newId
     *
     * @return integer
     */
    public function getNewId()
    {
        return $this->newId;
    }

    /**
     * Set eventDate
     *
     * @param \DateTime $eventDate
     *
     * @return ZzAuditNodeService
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * Get eventDate
     *
     * @return \DateTime
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }
}
