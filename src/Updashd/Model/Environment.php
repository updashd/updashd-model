<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Environment
 *
 * @ORM\Table(name="environment", indexes={@ORM\Index(name="environment_name", columns={"environment_name"}), @ORM\Index(name="environment_account_id", columns={"account_id"}), @ORM\Index(name="environment_updater_id", columns={"updater_id"}), @ORM\Index(name="environment_creator_id", columns={"creator_id"})})
 * @ORM\Entity
 */
class Environment extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="environment_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $environmentId;

    /**
     * @var string
     *
     * @ORM\Column(name="environment_name", type="string", length=50, nullable=false)
     */
    private $environmentName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="bootstrap_color", type="string", length=20, nullable=false)
     */
    private $bootstrapColor = 'info';

    /**
     * @var string
     *
     * @ORM\Column(name="layout_color", type="string", length=20, nullable=false)
     */
    private $layoutColor = 'aqua';

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder = '0';

    /**
     * @var \Updashd\Model\Account
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     * })
     */
    private $account;

    /**
     * @var \Updashd\Model\Node[]
     *
     * @ORM\OneToMany(targetEntity="\Updashd\Model\Node", mappedBy="environment")
     */
    private $nodes;


    /**
     * Get environmentId
     *
     * @return integer
     */
    public function getEnvironmentId()
    {
        return $this->environmentId;
    }

    /**
     * Set environmentName
     *
     * @param string $environmentName
     *
     * @return Environment
     */
    public function setEnvironmentName($environmentName)
    {
        $this->environmentName = $environmentName;

        return $this;
    }

    /**
     * Get environmentName
     *
     * @return string
     */
    public function getEnvironmentName()
    {
        return $this->environmentName;
    }

    /**
     * Set bootstrapColor
     *
     * @param string $bootstrapColor
     *
     * @return Environment
     */
    public function setBootstrapColor($bootstrapColor)
    {
        $this->bootstrapColor = $bootstrapColor;

        return $this;
    }

    /**
     * Get bootstrapColor
     *
     * @return string
     */
    public function getBootstrapColor()
    {
        return $this->bootstrapColor;
    }

    /**
     * Set layoutColor
     *
     * @param string $layoutColor
     *
     * @return Environment
     */
    public function setLayoutColor($layoutColor)
    {
        $this->layoutColor = $layoutColor;

        return $this;
    }

    /**
     * Get layoutColor
     *
     * @return string
     */
    public function getLayoutColor()
    {
        return $this->layoutColor;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return Environment
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set account
     *
     * @param \Updashd\Model\Account $account
     *
     * @return Environment
     */
    public function setAccount(\Updashd\Model\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Updashd\Model\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return \Updashd\Model\Node[]
     */
    public function getNodes () {
        return $this->nodes;
    }
}
