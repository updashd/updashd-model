<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Zone
 *
 * @ORM\Table(name="zone", indexes={@ORM\Index(name="zone_name", columns={"zone_name"}), @ORM\Index(name="zone_account_id", columns={"account_id"}), @ORM\Index(name="zone_updater_id", columns={"updater_id"}), @ORM\Index(name="zone_creator_id", columns={"creator_id"})})
 * @ORM\Entity
 */
class Zone extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="zone_id", type="string", length=10, nullable=false)
     * @ORM\Id
     */
    private $zoneId;

    /**
     * @var string
     *
     * @ORM\Column(name="zone_name", type="string", length=50, nullable=false)
     */
    private $zoneName = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder = '0';

    /**
     * @var \Updashd\Model\Account
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     * })
     */
    private $account;



    /**
     * Set zoneId
     *
     * @param string $zoneId
     *
     * @return Zone
     */
    public function setZoneId($zoneId)
    {
        $this->zoneId = $zoneId;

        return $this;
    }

    /**
     * Get zoneId
     *
     * @return string
     */
    public function getZoneId()
    {
        return $this->zoneId;
    }

    /**
     * Set zoneName
     *
     * @param string $zoneName
     *
     * @return Zone
     */
    public function setZoneName($zoneName)
    {
        $this->zoneName = $zoneName;

        return $this;
    }

    /**
     * Get zoneName
     *
     * @return string
     */
    public function getZoneName()
    {
        return $this->zoneName;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return Zone
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set account
     *
     * @param \Updashd\Model\Account $account
     *
     * @return Zone
     */
    public function setAccount(\Updashd\Model\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Updashd\Model\Account
     */
    public function getAccount()
    {
        return $this->account;
    }
}
