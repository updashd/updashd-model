<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonSetting
 *
 * @ORM\Table(name="person_setting", indexes={@ORM\Index(name="person_setting_creator_id_fk", columns={"creator_id"}), @ORM\Index(name="person_setting_updater_id_fk", columns={"updater_id"}), @ORM\Index(name="person_setting_setting_setting_id_fk", columns={"setting_id"}), @ORM\Index(name="person_setting_key", columns={"person_id", "setting_id"}), @ORM\Index(name="IDX_BDEA0B11217BBB47", columns={"person_id"})})
 * @ORM\Entity
 */
class PersonSetting extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="person_setting_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $personSettingId;

    /**
     * @var string
     *
     * @ORM\Column(name="setting_value", type="string", length=255, nullable=true)
     */
    private $settingValue;

    /**
     * @var \Updashd\Model\Person
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Person")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
     * })
     */
    private $person;

    /**
     * @var \Updashd\Model\Setting
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Setting")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="setting_id", referencedColumnName="setting_id")
     * })
     */
    private $setting;



    /**
     * Get personSettingId
     *
     * @return integer
     */
    public function getPersonSettingId()
    {
        return $this->personSettingId;
    }

    /**
     * Set settingValue
     *
     * @param string $settingValue
     *
     * @return PersonSetting
     */
    public function setSettingValue($settingValue)
    {
        $this->settingValue = $settingValue;

        return $this;
    }

    /**
     * Get settingValue
     *
     * @return string
     */
    public function getSettingValue()
    {
        return $this->settingValue;
    }

    /**
     * Set person
     *
     * @param \Updashd\Model\Person $person
     *
     * @return PersonSetting
     */
    public function setPerson(\Updashd\Model\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \Updashd\Model\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set setting
     *
     * @param \Updashd\Model\Setting $setting
     *
     * @return PersonSetting
     */
    public function setSetting(\Updashd\Model\Setting $setting = null)
    {
        $this->setting = $setting;

        return $this;
    }

    /**
     * Get setting
     *
     * @return \Updashd\Model\Setting
     */
    public function getSetting()
    {
        return $this->setting;
    }
}
