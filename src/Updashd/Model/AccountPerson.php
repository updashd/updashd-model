<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccountPerson
 *
 * @ORM\Table(name="account_person", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_account_person_id", columns={"account_id", "person_id"})}, indexes={@ORM\Index(name="account_person_creator_id", columns={"creator_id"}), @ORM\Index(name="account_person_updater_id", columns={"updater_id"}), @ORM\Index(name="account_person_person_id_idx", columns={"person_id"}), @ORM\Index(name="IDX_13E81F059B6B5FBA", columns={"account_id"})})
 * @ORM\Entity
 */
class AccountPerson extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="account_person_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $accountPersonId;

    /**
     * @var string
     *
     * @ORM\Column(name="role_type", type="string", length=15, nullable=false)
     */
    private $roleType = 'VIEWER';

    /**
     * @var \Updashd\Model\Account
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     * })
     */
    private $account;

    /**
     * @var \Updashd\Model\Person
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Person")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
     * })
     */
    private $person;



    /**
     * Get accountPersonId
     *
     * @return integer
     */
    public function getAccountPersonId()
    {
        return $this->accountPersonId;
    }

    /**
     * Set roleType
     *
     * @param string $roleType
     *
     * @return AccountPerson
     */
    public function setRoleType($roleType)
    {
        $this->roleType = $roleType;

        return $this;
    }

    /**
     * Get roleType
     *
     * @return string
     */
    public function getRoleType()
    {
        return $this->roleType;
    }

    /**
     * Set account
     *
     * @param \Updashd\Model\Account $account
     *
     * @return AccountPerson
     */
    public function setAccount(\Updashd\Model\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Updashd\Model\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set person
     *
     * @param \Updashd\Model\Person $person
     *
     * @return AccountPerson
     */
    public function setPerson(\Updashd\Model\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \Updashd\Model\Person
     */
    public function getPerson()
    {
        return $this->person;
    }
}
