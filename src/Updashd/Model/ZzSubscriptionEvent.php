<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * ZzSubscriptionEvent
 *
 * @ORM\Table(name="zz_subscription_event", indexes={@ORM\Index(name="zz_sub_event_client_type", columns={"client_id", "db_event_type"}), @ORM\Index(name="zz_subscription_event_zz_subscription_subscription_id_fk", columns={"subscription_id"})})
 * @ORM\Entity
 */
class ZzSubscriptionEvent extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $eventId;

    /**
     * @var string
     *
     * @ORM\Column(name="client_id", type="string", length=10, nullable=false)
     */
    private $clientId;

    /**
     * @var string
     *
     * @ORM\Column(name="db_table_name", type="string", length=50, nullable=false)
     */
    private $dbTableName;

    /**
     * @var string
     *
     * @ORM\Column(name="db_event_type", type="string", length=1, nullable=false)
     */
    private $dbEventType = 'I';

    /**
     * @var integer
     *
     * @ORM\Column(name="zz_audit_id", type="integer", nullable=false)
     */
    private $zzAuditId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="event_date", type="datetime", nullable=false)
     */
    private $eventDate;

    /**
     * @var \Updashd\Model\ZzSubscription
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\ZzSubscription")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subscription_id", referencedColumnName="subscription_id")
     * })
     */
    private $subscription;



    /**
     * Get eventId
     *
     * @return integer
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * Set clientId
     *
     * @param string $clientId
     *
     * @return ZzSubscriptionEvent
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId
     *
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set dbTableName
     *
     * @param string $dbTableName
     *
     * @return ZzSubscriptionEvent
     */
    public function setDbTableName($dbTableName)
    {
        $this->dbTableName = $dbTableName;

        return $this;
    }

    /**
     * Get dbTableName
     *
     * @return string
     */
    public function getDbTableName()
    {
        return $this->dbTableName;
    }

    /**
     * Set dbEventType
     *
     * @param string $dbEventType
     *
     * @return ZzSubscriptionEvent
     */
    public function setDbEventType($dbEventType)
    {
        $this->dbEventType = $dbEventType;

        return $this;
    }

    /**
     * Get dbEventType
     *
     * @return string
     */
    public function getDbEventType()
    {
        return $this->dbEventType;
    }

    /**
     * Set zzAuditId
     *
     * @param integer $zzAuditId
     *
     * @return ZzSubscriptionEvent
     */
    public function setZzAuditId($zzAuditId)
    {
        $this->zzAuditId = $zzAuditId;

        return $this;
    }

    /**
     * Get zzAuditId
     *
     * @return integer
     */
    public function getZzAuditId()
    {
        return $this->zzAuditId;
    }

    /**
     * Set eventDate
     *
     * @param \DateTime $eventDate
     *
     * @return ZzSubscriptionEvent
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * Get eventDate
     *
     * @return \DateTime
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * Set subscription
     *
     * @param \Updashd\Model\ZzSubscription $subscription
     *
     * @return ZzSubscriptionEvent
     */
    public function setSubscription(\Updashd\Model\ZzSubscription $subscription = null)
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * Get subscription
     *
     * @return \Updashd\Model\ZzSubscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }
}
