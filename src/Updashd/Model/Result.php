<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Result
 *
 * @ORM\Table(name="result", indexes={@ORM\Index(name="result_incident_incident_id_fk", columns={"incident_id"}), @ORM\Index(name="result_node_service_zone_index", columns={"node_service_id", "zone_id"}), @ORM\Index(name="result_node_service_zone_start_time_index", columns={"node_service_id", "zone_id", "start_time"}), @ORM\Index(name="result_node_service_zone_status_code_index", columns={"node_service_id", "zone_id", "status_code"}), @ORM\Index(name="result_nsz_index", columns={"node_service_id", "zone_id"}), @ORM\Index(name="result_nsz_start_time_index", columns={"node_service_id", "zone_id", "start_time"}), @ORM\Index(name="result_nsz_status_code_index", columns={"node_service_id", "zone_id", "status_code"}), @ORM\Index(name="result_nsz_status_time_index", columns={"node_service_id", "zone_id", "status_code", "start_time"}), @ORM\Index(name="result_person_creator_id_fk", columns={"creator_id"}), @ORM\Index(name="result_person_updater_id_fk", columns={"updater_id"}), @ORM\Index(name="result_severity_severity_id_fk", columns={"status_code"}), @ORM\Index(name="result_zone_zone_id_fk", columns={"zone_id"}), @ORM\Index(name="IDX_136AC11339FC260C", columns={"node_service_id"})})
 * @ORM\Entity
 */
class Result extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="result_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $resultId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="datetime", nullable=false)
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_time", type="datetime", nullable=false)
     */
    private $endTime;

    /**
     * @var float
     *
     * @ORM\Column(name="elapsed_time", type="float", precision=10, scale=0, nullable=false)
     */
    private $elapsedTime;

    /**
     * @var \Updashd\Model\Incident
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Incident")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="incident_id", referencedColumnName="incident_id")
     * })
     */
    private $incident;

    /**
     * @var \Updashd\Model\NodeService
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\NodeService")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="node_service_id", referencedColumnName="node_service_id")
     * })
     */
    private $nodeService;

    /**
     * @var \Updashd\Model\Severity
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Severity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_code", referencedColumnName="severity_id")
     * })
     */
    private $statusCode;

    /**
     * @var \Updashd\Model\Zone
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Zone")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="zone_id", referencedColumnName="zone_id")
     * })
     */
    private $zone;

    /**
     * @var \Updashd\Model\ResultMetric[]
     *
     * @ORM\OneToMany(targetEntity="\Updashd\Model\ResultMetric", mappedBy="result")
     */
    private $resultMetrics;


    /**
     * Get resultId
     *
     * @return integer
     */
    public function getResultId()
    {
        return $this->resultId;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return Result
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     *
     * @return Result
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set elapsedTime
     *
     * @param float $elapsedTime
     *
     * @return Result
     */
    public function setElapsedTime($elapsedTime)
    {
        $this->elapsedTime = $elapsedTime;

        return $this;
    }

    /**
     * Get elapsedTime
     *
     * @return float
     */
    public function getElapsedTime()
    {
        return $this->elapsedTime;
    }

    /**
     * Set incident
     *
     * @param \Updashd\Model\Incident $incident
     *
     * @return Result
     */
    public function setIncident(\Updashd\Model\Incident $incident = null)
    {
        $this->incident = $incident;

        return $this;
    }

    /**
     * Get incident
     *
     * @return \Updashd\Model\Incident
     */
    public function getIncident()
    {
        return $this->incident;
    }

    /**
     * Set nodeService
     *
     * @param \Updashd\Model\NodeService $nodeService
     *
     * @return Result
     */
    public function setNodeService(\Updashd\Model\NodeService $nodeService = null)
    {
        $this->nodeService = $nodeService;

        return $this;
    }

    /**
     * Get nodeService
     *
     * @return \Updashd\Model\NodeService
     */
    public function getNodeService()
    {
        return $this->nodeService;
    }

    /**
     * Set statusCode
     *
     * @param \Updashd\Model\Severity $statusCode
     *
     * @return Result
     */
    public function setStatusCode(\Updashd\Model\Severity $statusCode = null)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Get statusCode
     *
     * @return \Updashd\Model\Severity
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set zone
     *
     * @param \Updashd\Model\Zone $zone
     *
     * @return Result
     */
    public function setZone(\Updashd\Model\Zone $zone = null)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone
     *
     * @return \Updashd\Model\Zone
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * @return \Updashd\Model\ResultMetric[]
     */
    public function getResultMetrics () {
        return $this->resultMetrics;
    }
}
