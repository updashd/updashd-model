<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * IncidentNotifierHistory
 *
 * @ORM\Table(name="incident_notifier_history", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_inh_incident_account_notifier", columns={"incident_id", "account_notifier_id"})}, indexes={@ORM\Index(name="inh_creator_id", columns={"creator_id"}), @ORM\Index(name="inh_updater_id", columns={"updater_id"}), @ORM\Index(name="inh_account_notifier_id", columns={"account_notifier_id"}), @ORM\Index(name="IDX_CE913B9C59E53FB9", columns={"incident_id"})})
 * @ORM\Entity
 */
class IncidentNotifierHistory extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="incident_notifier_history_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $incidentNotifierHistoryId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_notice", type="datetime", nullable=false)
     */
    private $lastNotice;

    /**
     * @var \Updashd\Model\AccountNotifier
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\AccountNotifier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_notifier_id", referencedColumnName="account_notifier_id")
     * })
     */
    private $accountNotifier;

    /**
     * @var \Updashd\Model\Incident
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Incident")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="incident_id", referencedColumnName="incident_id")
     * })
     */
    private $incident;



    /**
     * Get incidentNotifierHistoryId
     *
     * @return integer
     */
    public function getIncidentNotifierHistoryId()
    {
        return $this->incidentNotifierHistoryId;
    }

    /**
     * Set lastNotice
     *
     * @param \DateTime $lastNotice
     *
     * @return IncidentNotifierHistory
     */
    public function setLastNotice($lastNotice)
    {
        $this->lastNotice = $lastNotice;

        return $this;
    }

    /**
     * Get lastNotice
     *
     * @return \DateTime
     */
    public function getLastNotice()
    {
        return $this->lastNotice;
    }

    /**
     * Set accountNotifier
     *
     * @param \Updashd\Model\AccountNotifier $accountNotifier
     *
     * @return IncidentNotifierHistory
     */
    public function setAccountNotifier(\Updashd\Model\AccountNotifier $accountNotifier = null)
    {
        $this->accountNotifier = $accountNotifier;

        return $this;
    }

    /**
     * Get accountNotifier
     *
     * @return \Updashd\Model\AccountNotifier
     */
    public function getAccountNotifier()
    {
        return $this->accountNotifier;
    }

    /**
     * Set incident
     *
     * @param \Updashd\Model\Incident $incident
     *
     * @return IncidentNotifierHistory
     */
    public function setIncident(\Updashd\Model\Incident $incident = null)
    {
        $this->incident = $incident;

        return $this;
    }

    /**
     * Get incident
     *
     * @return \Updashd\Model\Incident
     */
    public function getIncident()
    {
        return $this->incident;
    }
}
