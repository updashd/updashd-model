<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * NodeService
 *
 * @ORM\Table(name="node_service", indexes={@ORM\Index(name="node_service_node_id", columns={"node_id"}), @ORM\Index(name="node_service_service", columns={"service_id"}), @ORM\Index(name="ns_node_service", columns={"node_id", "service_id"}), @ORM\Index(name="ns_node_service_enabled", columns={"node_id", "service_id", "is_enabled"}), @ORM\Index(name="ns_updater_id", columns={"updater_id"}), @ORM\Index(name="ns_creator_id", columns={"creator_id"})})
 * @ORM\Entity
 */
class NodeService extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="node_service_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $nodeServiceId;

    /**
     * @var string
     *
     * @ORM\Column(name="node_service_name", type="string", length=50, nullable=false)
     */
    private $nodeServiceName = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_enabled", type="boolean", nullable=false)
     */
    private $isEnabled = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder;

    /**
     * @var string
     *
     * @ORM\Column(name="schedule_type", type="string", length=15, nullable=true)
     */
    private $scheduleType = 'once';

    /**
     * @var string
     *
     * @ORM\Column(name="schedule_value", type="string", length=20, nullable=false)
     */
    private $scheduleValue = 'once';

    /**
     * @var string
     *
     * @ORM\Column(name="config", type="text", length=16777215, nullable=true)
     */
    private $config;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=8, nullable=false)
     */
    private $color = 'FF0000';

    /**
     * @var \Updashd\Model\Node
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Node", inversedBy="nodeServices")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="node_id", referencedColumnName="node_id")
     * })
     */
    private $node;

    /**
     * @var \Updashd\Model\Service
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Service")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="service_id", referencedColumnName="service_id")
     * })
     */
    private $service;

    /**
     * @var \Updashd\Model\NodeServiceZone[]
     *
     * @ORM\OneToMany(targetEntity="\Updashd\Model\NodeServiceZone", mappedBy="nodeService")
     */
    private $nodeServiceZones;


    /**
     * Get nodeServiceId
     *
     * @return integer
     */
    public function getNodeServiceId()
    {
        return $this->nodeServiceId;
    }

    /**
     * Set nodeServiceName
     *
     * @param string $nodeServiceName
     *
     * @return NodeService
     */
    public function setNodeServiceName($nodeServiceName)
    {
        $this->nodeServiceName = $nodeServiceName;

        return $this;
    }

    /**
     * Get nodeServiceName
     *
     * @return string
     */
    public function getNodeServiceName()
    {
        return $this->nodeServiceName;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return NodeService
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return NodeService
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set scheduleType
     *
     * @param string $scheduleType
     *
     * @return NodeService
     */
    public function setScheduleType($scheduleType)
    {
        $this->scheduleType = $scheduleType;

        return $this;
    }

    /**
     * Get scheduleType
     *
     * @return string
     */
    public function getScheduleType()
    {
        return $this->scheduleType;
    }

    /**
     * Set scheduleValue
     *
     * @param string $scheduleValue
     *
     * @return NodeService
     */
    public function setScheduleValue($scheduleValue)
    {
        $this->scheduleValue = $scheduleValue;

        return $this;
    }

    /**
     * Get scheduleValue
     *
     * @return string
     */
    public function getScheduleValue()
    {
        return $this->scheduleValue;
    }

    /**
     * Set config
     *
     * @param string $config
     *
     * @return NodeService
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Get config
     *
     * @return string
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return NodeService
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set node
     *
     * @param \Updashd\Model\Node $node
     *
     * @return NodeService
     */
    public function setNode(\Updashd\Model\Node $node = null)
    {
        $this->node = $node;

        return $this;
    }

    /**
     * Get node
     *
     * @return \Updashd\Model\Node
     */
    public function getNode()
    {
        return $this->node;
    }

    /**
     * Set service
     *
     * @param \Updashd\Model\Service $service
     *
     * @return NodeService
     */
    public function setService(\Updashd\Model\Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \Updashd\Model\Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @return \Updashd\Model\NodeServiceZone[]
     */
    public function getNodeServiceZones () {
        return $this->nodeServiceZones;
    }
}
