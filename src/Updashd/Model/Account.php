<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Account
 *
 * @ORM\Table(name="account", indexes={@ORM\Index(name="account_name", columns={"name"}), @ORM\Index(name="account_updater_id", columns={"updater_id"}), @ORM\Index(name="account_creator_id", columns={"creator_id"}), @ORM\Index(name="account_owner_id_idx", columns={"owner_id"})})
 * @ORM\Entity
 */
class Account extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="account_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $accountId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=30, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_url", type="string", length=200, nullable=false)
     */
    private $logoUrl = '';

    /**
     * @var \Updashd\Model\Person
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Person")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id", referencedColumnName="person_id")
     * })
     */
    private $owner;



    /**
     * Get accountId
     *
     * @return integer
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Account
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Account
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set logoUrl
     *
     * @param string $logoUrl
     *
     * @return Account
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;

        return $this;
    }

    /**
     * Get logoUrl
     *
     * @return string
     */
    public function getLogoUrl()
    {
        return $this->logoUrl;
    }

    /**
     * Set owner
     *
     * @param \Updashd\Model\Person $owner
     *
     * @return Account
     */
    public function setOwner(\Updashd\Model\Person $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \Updashd\Model\Person
     */
    public function getOwner()
    {
        return $this->owner;
    }
}
