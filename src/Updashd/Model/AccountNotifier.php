<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccountNotifier
 *
 * @ORM\Table(name="account_notifier", indexes={@ORM\Index(name="ac_account_id", columns={"account_id"}), @ORM\Index(name="ac_sort_order", columns={"sort_order"}), @ORM\Index(name="ac_notifier_id", columns={"notifier_id"}), @ORM\Index(name="ac_updater_id", columns={"updater_id"}), @ORM\Index(name="ac_creator_id", columns={"creator_id"})})
 * @ORM\Entity
 */
class AccountNotifier extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="account_notifier_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $accountNotifierId;

    /**
     * @var string
     *
     * @ORM\Column(name="account_notifier_name", type="string", length=50, nullable=false)
     */
    private $accountNotifierName = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_enabled", type="boolean", nullable=false)
     */
    private $isEnabled = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_default", type="boolean", nullable=false)
     */
    private $isDefault = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="frequency", type="integer", nullable=false)
     */
    private $frequency = '3600';

    /**
     * @var string
     *
     * @ORM\Column(name="config", type="text", nullable=true)
     */
    private $config;

    /**
     * @var \Updashd\Model\Account
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     * })
     */
    private $account;

    /**
     * @var \Updashd\Model\Notifier
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Notifier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="notifier_id", referencedColumnName="notifier_id")
     * })
     */
    private $notifier;



    /**
     * Get accountNotifierId
     *
     * @return integer
     */
    public function getAccountNotifierId()
    {
        return $this->accountNotifierId;
    }

    /**
     * Set accountNotifierName
     *
     * @param string $accountNotifierName
     *
     * @return AccountNotifier
     */
    public function setAccountNotifierName($accountNotifierName)
    {
        $this->accountNotifierName = $accountNotifierName;

        return $this;
    }

    /**
     * Get accountNotifierName
     *
     * @return string
     */
    public function getAccountNotifierName()
    {
        return $this->accountNotifierName;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return AccountNotifier
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Set isDefault
     *
     * @param boolean $isDefault
     *
     * @return AccountNotifier
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return AccountNotifier
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set frequency
     *
     * @param integer $frequency
     *
     * @return AccountNotifier
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return integer
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set config
     *
     * @param string $config
     *
     * @return AccountNotifier
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Get config
     *
     * @return string
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set account
     *
     * @param \Updashd\Model\Account $account
     *
     * @return AccountNotifier
     */
    public function setAccount(\Updashd\Model\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Updashd\Model\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set notifier
     *
     * @param \Updashd\Model\Notifier $notifier
     *
     * @return AccountNotifier
     */
    public function setNotifier(\Updashd\Model\Notifier $notifier = null)
    {
        $this->notifier = $notifier;

        return $this;
    }

    /**
     * Get notifier
     *
     * @return \Updashd\Model\Notifier
     */
    public function getNotifier()
    {
        return $this->notifier;
    }
}
