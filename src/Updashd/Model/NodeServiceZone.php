<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * NodeServiceZone
 *
 * @ORM\Table(name="node_service_zone", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_node_service_zone", columns={"node_service_id", "zone_id"})}, indexes={@ORM\Index(name="nsz_node_service_id", columns={"node_service_id"}), @ORM\Index(name="nsz_zone_id", columns={"zone_id"}), @ORM\Index(name="nsz_updater_id", columns={"updater_id"}), @ORM\Index(name="nsz_creator_id", columns={"creator_id"})})
 * @ORM\Entity
 */
class NodeServiceZone extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="node_service_zone_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $nodeServiceZoneId;

    /**
     * @var \Updashd\Model\NodeService
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\NodeService", inversedBy="nodeServiceZones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="node_service_id", referencedColumnName="node_service_id")
     * })
     */
    private $nodeService;

    /**
     * @var \Updashd\Model\Zone
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Zone")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="zone_id", referencedColumnName="zone_id")
     * })
     */
    private $zone;



    /**
     * Get nodeServiceZoneId
     *
     * @return integer
     */
    public function getNodeServiceZoneId()
    {
        return $this->nodeServiceZoneId;
    }

    /**
     * Set nodeService
     *
     * @param \Updashd\Model\NodeService $nodeService
     *
     * @return NodeServiceZone
     */
    public function setNodeService(\Updashd\Model\NodeService $nodeService = null)
    {
        $this->nodeService = $nodeService;

        return $this;
    }

    /**
     * Get nodeService
     *
     * @return \Updashd\Model\NodeService
     */
    public function getNodeService()
    {
        return $this->nodeService;
    }

    /**
     * Set zone
     *
     * @param \Updashd\Model\Zone $zone
     *
     * @return NodeServiceZone
     */
    public function setZone(\Updashd\Model\Zone $zone = null)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone
     *
     * @return \Updashd\Model\Zone
     */
    public function getZone()
    {
        return $this->zone;
    }
}
