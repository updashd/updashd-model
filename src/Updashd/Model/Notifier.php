<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notifier
 *
 * @ORM\Table(name="notifier", indexes={@ORM\Index(name="notifier_sort_order_index", columns={"sort_order"}), @ORM\Index(name="notifier_module_name_index", columns={"module_name"}), @ORM\Index(name="notifier_notifier_name_index", columns={"notifier_name"}), @ORM\Index(name="notifier_account_account_id_fk", columns={"account_id"}), @ORM\Index(name="notifier_updater_id_fk", columns={"updater_id"}), @ORM\Index(name="notifier_creator_id_fk", columns={"creator_id"})})
 * @ORM\Entity
 */
class Notifier extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="notifier_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $notifierId;

    /**
     * @var string
     *
     * @ORM\Column(name="notifier_name", type="string", length=50, nullable=false)
     */
    private $notifierName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="module_name", type="string", length=20, nullable=false)
     */
    private $moduleName = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder = '0';

    /**
     * @var \Updashd\Model\Account
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     * })
     */
    private $account;



    /**
     * Get notifierId
     *
     * @return integer
     */
    public function getNotifierId()
    {
        return $this->notifierId;
    }

    /**
     * Set notifierName
     *
     * @param string $notifierName
     *
     * @return Notifier
     */
    public function setNotifierName($notifierName)
    {
        $this->notifierName = $notifierName;

        return $this;
    }

    /**
     * Get notifierName
     *
     * @return string
     */
    public function getNotifierName()
    {
        return $this->notifierName;
    }

    /**
     * Set moduleName
     *
     * @param string $moduleName
     *
     * @return Notifier
     */
    public function setModuleName($moduleName)
    {
        $this->moduleName = $moduleName;

        return $this;
    }

    /**
     * Get moduleName
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return Notifier
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set account
     *
     * @param \Updashd\Model\Account $account
     *
     * @return Notifier
     */
    public function setAccount(\Updashd\Model\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Updashd\Model\Account
     */
    public function getAccount()
    {
        return $this->account;
    }
}
