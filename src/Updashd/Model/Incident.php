<?php

namespace Updashd\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Incident
 *
 * @ORM\Table(name="incident", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_incident_occurrence", columns={"node_service_id", "zone_id", "date_first_seen"})}, indexes={@ORM\Index(name="incident_severity", columns={"severity"}), @ORM\Index(name="incident_date", columns={"date_first_seen"}), @ORM\Index(name="incident_node_service_zone", columns={"node_service_id", "zone_id"}), @ORM\Index(name="incident_updater_id", columns={"updater_id"}), @ORM\Index(name="incident_creator_id", columns={"creator_id"}), @ORM\Index(name="incident_zone_id", columns={"zone_id"}), @ORM\Index(name="incident_account_id", columns={"account_id"}), @ORM\Index(name="incident_severity_severity_id_fk", columns={"severity"}), @ORM\Index(name="IDX_3D03A11A39FC260C", columns={"node_service_id"})})
 * @ORM\Entity
 */
class Incident extends \Updashd\Model\AbstractAuditedEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="incident_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $incidentId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_first_seen", type="datetime", nullable=false)
     */
    private $dateFirstSeen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_last_seen", type="datetime", nullable=false)
     */
    private $dateLastSeen;

    /**
     * @var integer
     *
     * @ORM\Column(name="message_code", type="integer", nullable=true)
     */
    private $messageCode;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", length=16777215, nullable=false)
     */
    private $message;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_read", type="boolean", nullable=false)
     */
    private $isRead = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_resolved", type="boolean", nullable=false)
     */
    private $isResolved = '0';

    /**
     * @var \Updashd\Model\Account
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     * })
     */
    private $account;

    /**
     * @var \Updashd\Model\NodeService
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\NodeService")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="node_service_id", referencedColumnName="node_service_id")
     * })
     */
    private $nodeService;

    /**
     * @var \Updashd\Model\Severity
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Severity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="severity", referencedColumnName="severity_id")
     * })
     */
    private $severity;

    /**
     * @var \Updashd\Model\Zone
     *
     * @ORM\ManyToOne(targetEntity="Updashd\Model\Zone")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="zone_id", referencedColumnName="zone_id")
     * })
     */
    private $zone;

    /**
     * @var \Updashd\Model\Result[]
     *
     * @ORM\OneToMany(targetEntity="\Updashd\Model\Result", mappedBy="incident")
     */
    private $results;

    /**
     * Get incidentId
     *
     * @return integer
     */
    public function getIncidentId()
    {
        return $this->incidentId;
    }

    /**
     * Set dateFirstSeen
     *
     * @param \DateTime $dateFirstSeen
     *
     * @return Incident
     */
    public function setDateFirstSeen($dateFirstSeen)
    {
        $this->dateFirstSeen = $dateFirstSeen;

        return $this;
    }

    /**
     * Get dateFirstSeen
     *
     * @return \DateTime
     */
    public function getDateFirstSeen()
    {
        return $this->dateFirstSeen;
    }

    /**
     * Set dateLastSeen
     *
     * @param \DateTime $dateLastSeen
     *
     * @return Incident
     */
    public function setDateLastSeen($dateLastSeen)
    {
        $this->dateLastSeen = $dateLastSeen;

        return $this;
    }

    /**
     * Get dateLastSeen
     *
     * @return \DateTime
     */
    public function getDateLastSeen()
    {
        return $this->dateLastSeen;
    }

    /**
     * Set messageCode
     *
     * @param integer $messageCode
     *
     * @return Incident
     */
    public function setMessageCode($messageCode)
    {
        $this->messageCode = $messageCode;

        return $this;
    }

    /**
     * Get messageCode
     *
     * @return integer
     */
    public function getMessageCode()
    {
        return $this->messageCode;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Incident
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set isRead
     *
     * @param boolean $isRead
     *
     * @return Incident
     */
    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;

        return $this;
    }

    /**
     * Get isRead
     *
     * @return boolean
     */
    public function getIsRead()
    {
        return $this->isRead;
    }

    /**
     * Set isResolved
     *
     * @param boolean $isResolved
     *
     * @return Incident
     */
    public function setIsResolved($isResolved)
    {
        $this->isResolved = $isResolved;

        return $this;
    }

    /**
     * Get isResolved
     *
     * @return boolean
     */
    public function getIsResolved()
    {
        return $this->isResolved;
    }

    /**
     * Set account
     *
     * @param \Updashd\Model\Account $account
     *
     * @return Incident
     */
    public function setAccount(\Updashd\Model\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Updashd\Model\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set nodeService
     *
     * @param \Updashd\Model\NodeService $nodeService
     *
     * @return Incident
     */
    public function setNodeService(\Updashd\Model\NodeService $nodeService = null)
    {
        $this->nodeService = $nodeService;

        return $this;
    }

    /**
     * Get nodeService
     *
     * @return \Updashd\Model\NodeService
     */
    public function getNodeService()
    {
        return $this->nodeService;
    }

    /**
     * Set severity
     *
     * @param \Updashd\Model\Severity $severity
     *
     * @return Incident
     */
    public function setSeverity(\Updashd\Model\Severity $severity = null)
    {
        $this->severity = $severity;

        return $this;
    }

    /**
     * Get severity
     *
     * @return \Updashd\Model\Severity
     */
    public function getSeverity()
    {
        return $this->severity;
    }

    /**
     * Set zone
     *
     * @param \Updashd\Model\Zone $zone
     *
     * @return Incident
     */
    public function setZone(\Updashd\Model\Zone $zone = null)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone
     *
     * @return \Updashd\Model\Zone
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * @return \Updashd\Model\Result[]
     */
    public function getResults () {
        return $this->results;
    }
}
