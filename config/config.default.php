<?php
return [
    'doctrine' => [
        'options' => [
            'dev_mode' => true,
            'simple_annotation' => false,
            'entity_paths' => [
                __DIR__ . '/../src/'
            ],
            'proxy_dir' => __DIR__ . '/../data/doctrine_proxy'
        ],
        'connection' => [
            'driver' => 'pdo_mysql',
            'host' => 'localhost',
            'dbname' => 'updashd',
            'user' => 'updashd',
            'port' => 3306,
            'password' => 'abc12345',
        ]
    ]
];