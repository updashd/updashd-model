<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "vendor/autoload.php";

$doctrineConfig = require __DIR__ . '/config/config.default.php';

if (file_exists(__DIR__ . '/config/config.local.php')) {
    $doctrineConfig = array_replace_recursive($doctrineConfig, require __DIR__ . '/config/config.local.php');
}

$doctrineConfig = $doctrineConfig['doctrine'];

$config = Setup::createAnnotationMetadataConfiguration(
    $doctrineConfig['options']['entity_paths'],
    $doctrineConfig['options']['dev_mode'],
    $doctrineConfig['options']['proxy_dir'],
    null, // Ignore cache for CLI
    $doctrineConfig['options']['simple_annotation']
);

$entityManager = EntityManager::create($doctrineConfig['connection'], $config);

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);

/*
 * ./vendor/bin/doctrine orm:convert-mapping -f --from-database --extend "Updashd\\Model\\AbstractAuditedEntity" --namespace "Updashd\\Model\\" -- annotation src/
 * ./vendor/bin/doctrine orm:generate-entities --generate-annotations=true src
 */