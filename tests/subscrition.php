<?php
require __DIR__ . '/../vendor/autoload.php';

$doctrineConfig = require __DIR__ . '/../config/config.default.php';

if (file_exists(__DIR__ . '/../config/config.local.php')) {
    $doctrineConfig = array_replace_recursive($doctrineConfig, require __DIR__ . '/../config/config.local.php');
}

$doctrineConfig = $doctrineConfig['doctrine'];

$manager = new \Updashd\Doctrine\Manager($doctrineConfig);

$em = $manager->getEntityManager();
$pdo = $em->getConnection();

$subscriptions = new \Updashd\DbEvent\Subscriptions();
$subscriptions->addSubscriptionAuto('node', ['I', 'U']);
$subscriptions->addSubscriptionAuto('service', ['I', 'U']);
$subscriptions->addSubscriptionAuto('zone', ['I', 'U']);
$subscriptions->addSubscriptionAuto('node_service', ['I', 'U']);
$subscriptions->addSubscriptionAuto('node_service_zone', ['I', 'U', 'D']);
$processor = new \Updashd\DbEvent\Processor($pdo, 'TST', 'tmp_tst', $subscriptions);
$popStmts = new \Updashd\DbEvent\PopulateStatements();
$popStmts
    ->addStatement(new \Updashd\DbEvent\PopulateStatement(
        'nsz.node_service_zone_id',
        'node',
        null,
        [
            'INNER JOIN node n ON a.new_id = n.node_id',
            'INNER JOIN node_service ns ON ns.node_id = n.node_id',
            'INNER JOIN node_service_zone nsz ON nsz.node_service_id = ns.node_service_id',
        ],
        ['I', 'U']
    ))
    ->addStatement(new \Updashd\DbEvent\PopulateStatement(
        'nsz.node_service_zone_id',
        'service',
        null,
        [
            'INNER JOIN service s ON a.new_id = s.service_id',
            'INNER JOIN node_service ns ON ns.service_id = s.service_id',
            'INNER JOIN node_service_zone nsz ON nsz.node_service_id = ns.node_service_id',
        ],
        ['I', 'U']
    ))
    ->addStatement(new \Updashd\DbEvent\PopulateStatement(
        'nsz.node_service_zone_id',
        'node_service',
        null,
        [
            'INNER JOIN node_service ns ON a.new_id = ns.node_service_id',
            'INNER JOIN node_service_zone nsz ON nsz.node_service_id = ns.node_service_id',
        ],
        ['I', 'U']
    ))
    ->addStatement(new \Updashd\DbEvent\PopulateStatement(
        'nsz.node_service_zone_id',
        'zone',
        null,
        [
            'INNER JOIN node_service_zone nsz ON a.new_id = nsz.zone_id',
        ],
        ['I', 'U']
    ))
    ->addStatement(new \Updashd\DbEvent\PopulateStatement(
        'a.new_id',
        'node_service_zone',
        null,
        [],
        ['I', 'U']
    ))
    ->addStatement(new \Updashd\DbEvent\PopulateStatement(
        'a.old_id',
        'node_service_zone',
        null,
        [],
        ['D']
    ));

$processor->setPopulateStatements($popStmts);

$processor->populate();

$changes = $processor->retrieve(
    [
        'ns.node_service_id',
        'z.zone_id',
        '(' .
        '  IFNULL(n.is_enabled, 0) &' .
        '  IFNULL(ns.is_enabled, 0) &' .
        '  IF(ns.node_service_id IS NOT NULL, 1, 0)' .
        ') AS is_enabled',
        's.module_name',
        'ns.schedule_type',
        'ns.schedule_value',
        'n.hostname',
        'n.ip',
        'ns.config',
        'n.node_id',
        'a.account_id',
    ],
    [
        'LEFT JOIN node_service_zone nsz ON r.root_id = nsz.node_service_zone_id',
        'LEFT JOIN node_service ns ON nsz.node_service_id = ns.node_service_id',
        'LEFT JOIN service s ON ns.service_id = s.service_id',
        'LEFT JOIN node n ON ns.node_id = n.node_id',
        'LEFT JOIN zone z ON nsz.zone_id = z.zone_id',
        'LEFT JOIN account a ON n.account_id = a.account_id',
    ],
    'node_service_zone_id',
    'node_service_zone'
);

print_r($changes);

$processor->finalize();