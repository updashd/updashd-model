<?php
require __DIR__ . '/../vendor/autoload.php';

$doctrineConfig = require __DIR__ . '/../config/config.default.php';

if (file_exists(__DIR__ . '/../config/config.local.php')) {
    $doctrineConfig = array_replace_recursive($doctrineConfig, require __DIR__ . '/../config/config.local.php');
}

$doctrineConfig = $doctrineConfig['doctrine'];

$manager = new \Updashd\Doctrine\Manager($doctrineConfig);

$em = $manager->getEntityManager();
$resultRepo = $em->getRepository(\Updashd\Model\Result::class);

/** @var \Updashd\Model\Result $result */
$result = $resultRepo->findOneBy(['statusCode' => $em->getReference(\Updashd\Model\Severity::class, 'CRITICAL')]);

echo $result->getResultId();