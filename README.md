Updashd
=======

# Generating Doctrine Entities (For Contributors)

    ./vendor/bin/doctrine orm:convert-mapping -f --from-database --extend "Updashd\\Model\\AbstractAuditedEntity" --namespace "Updashd\\Model\\" -- annotation src/
    ./vendor/bin/doctrine orm:generate-entities --generate-annotations=true src
    
    