<?php

require 'vendor\\autoload.php';

$mappingCmd = new Updashd\Cli\Command('.\\vendor\\bin\\doctrine');
$mappingCmd->addParameterString('orm:convert-mapping');
$mappingCmd->addParameterFlag('-f');
$mappingCmd->addParameterFlag('--from-database');
$mappingCmd->addParameterFlagValue('--extend', 'Updashd\\Model\\AbstractAuditedEntity');
$mappingCmd->addParameterFlagValue('--namespace', 'Updashd\\Model\\');
$mappingCmd->addParameterString('--');
$mappingCmd->addParameterString('annotation');
$mappingCmd->addParameterString('src\\');
$mappingCmd->execute(true);

$entityCmd = new Updashd\Cli\Command('.\\vendor\\bin\\doctrine');
$entityCmd->addParameterString('orm:generate-entities');
$entityCmd->addParameterFlagValue('--generate-annotations', 'true', '=');
$entityCmd->addParameterString('src\\');
$entityCmd->execute(true);